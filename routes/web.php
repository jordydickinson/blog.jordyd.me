<?php

use \Illuminate\Http\Request;
use \Illuminate\Pagination\Paginator;
use \Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', function () {
    $stickies = App\Post::where('sticky', '=', true)->get()->filter(function (App\Post $post) {
        if (Auth::guest()) {
            return $post->audience === '*';
        } else {
            return Auth::user()->can('view', $post);
        }
    });
    $posts = App\Post::latest()->get()->filter(function (App\Post $post) {
        if (Auth::guest()) {
            return $post->audience === '*';
        } else {
            return Auth::user()->can('view', $post);
        }
    });
    $posts = new Paginator($posts, 10);
    return view('home', ['stickies' => $stickies, 'posts' => $posts]);
});

Route::get('/auth/login', function () {
    return view('auth.login');
})->name('login'); // For the Auth middleware.

Route::post('/auth/login', function (Request $request) {
    $credentials = [
        'email' => $request->input('email'),
        'password' => $request->input('password')
    ];
    if (Auth::attempt($credentials)) {
        return redirect()->intended('/home');
    }
    return redirect('/auth/failed');
});

Route::get('/auth/failed', function () {
    return view('auth.failed');
});

Route::get('/auth/logout', function () {
    Auth::logout();
    return redirect('/home');
});

Route::get('/post/new', function () {
    $post = new App\Post;
    $post->user_id = Auth::user()->id;
    $post->title = "Untitled";
    $post->summary = "Write a summary.";
    $post->body = "Write a body.";
    $post->sticky = false;
    $post->audience = '';
    $post->save();
    return view('post.edit', ['post' => $post]);
})->middleware('can:create,App\Post');

Route::get('/post/{post}/view', function (App\Post $post) {
    $can_view = $post->audience === '*'
        || (Auth::check() && Auth::user()->can('view', $post));
    if ($can_view) {
        return view('post.view', ['post' => $post]);
    }
    return abort(404);
});

Route::get('/post/{post}/edit', function (App\Post $post) {
    return view('post.edit', ['post' => $post]);
})->middleware('can:update,post');

Route::post('/post/{post}/edit', function (Request $request, App\Post $post) {
    $post->title = $request->input('title');
    $post->summary = $request->input('summary');
    $post->body = $request->input('body');
    $post->sticky = (bool)$request->input('sticky');
    $post->audience = $request->input('audience') ?: '';
    $post->save();
    return redirect('/home');
})->middleware('can:update,post');

Route::get('/post/{post}/delete', function (App\Post $post) {
    $post->delete();
    return redirect('/home');
})->middleware('can:delete,post');

Route::get('/gallery', function () {
    $image_storage_paths = Storage::files('public/gallery');
    $image_urls = [];
    foreach ($image_storage_paths as $path) {
        array_push($image_urls, '/gallery/' . basename($path));
    }
    return view('gallery', ['image_urls' => $image_urls]);
});

Route::get('/gallery/{filename}', function (string $filename) {
    return redirect(Storage::url('gallery/' . $filename));
});

Route::get('/gallery/{filename}/delete', function (string $filename) {
    if (Gate::allows('store')) {
        Storage::delete('public/gallery/' . $filename);
        return redirect('/gallery');
    }
    return abort(401);
});

Route::post('/gallery', function (Request $request) {
    if (Gate::allows('store')) {
        $file = $request->file('file');
        $filename = $request->input('filename');
        $file->storeAs('public/gallery', $filename);
        return redirect('/gallery');
    }
    return abort(401);
});
