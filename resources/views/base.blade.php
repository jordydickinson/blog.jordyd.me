<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/hot/styles.css">
    <title>@yield('title')</title>
</head>
<body>
    <header><nav id="main-nav">
        <a href="/home">Home</a>
        <a href="/gallery">Gallery</a>

        @can('create', App\Post::class)
            <a href="/post/new">New Post</a>
        @endcan

        @if (Auth::check())
            <a href="/auth/logout">Logout</a>
        @else
            <a href="/auth/login">Login</a>
        @endif
    </nav></header>

    <div id="content">
        @if (Auth::check())
            <p>Welcome, {{ Auth::user()->name }}.</p>
        @endif
        @yield('content')

        <footer class="license">@include('license')</footer>
    </div>

    <footer><nav id="page-nav">
            @yield('page-nav')
    </nav></footer>
</body>
</html>
