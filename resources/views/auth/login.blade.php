@extends('base')

@section('title', 'Login')

@section('content')
    <form method="post" action="/auth/login">
        {{ csrf_field() }}

        <input type="text" name="email" placeholder="YourEmail@Domain">
        <input type="password" name="password" placeholder="Password">
        <input type="submit" value="Login">
    </form>
@endsection
