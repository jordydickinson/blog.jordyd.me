<div class="post">
    <header>
        <h1>{{ $post->title }}</h1>
        <p class="summary">{{ $post->summary }}</p>
    </header>
    <footer>
        <nav>
            <a href="/post/{{ $post->id }}/view">Read More</a>
            @can('update', $post)
                <a href="/post/{{ $post->id }}/edit">Edit</a>
            @endcan
            @can('delete', $post)
                <a href="/post/{{ $post->id }}/delete"
                    onclick="return confirm('Really delete this post?');">Delete</a>
            @endcan
        </nav>
        @if (Auth::check() && $post->audience == Auth::user()->email)
            <p class="note">Only you can see this.</p>
        @endif
    </footer>
</div>
