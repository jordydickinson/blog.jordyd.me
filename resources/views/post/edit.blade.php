@extends('base')

@section('title', 'Edit Post')

@section('content')
    <form method="post" action="/post/{{ $post->id }}/edit">
        {{ csrf_field() }}
        <input type="text" name="title" value="{{ $post->title }}">
        <textarea name="summary" rows="3">{{ $post->summary }}</textarea>
        <textarea name="body" rows="40">{{ $post->body }}</textarea>
        <label>
            Audience:
            <input type="text" name="audience" value="{{ $post->audience }}" placeholder="Just me">
        </label>
        <label>
            <input type="checkbox" name="sticky" @if ($post->sticky) checked @endif>
            Sticky?
        </label>
        <input type="submit" value="Submit">
    </form>
@endsection
