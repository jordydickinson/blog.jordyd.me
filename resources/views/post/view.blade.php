@extends('base')

@section('head')
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script type="text/javascript">hljs.initHighlightingOnLoad();</script>
@endsection

@section('title', $post->title)

@section('content')
    <article class="post">
        <header>
            <h1>{{ $post->title }}</h1>
            <p class="summary">{{ $post->summary }}</p>
        </header>
        {!! Markdown::convertToHtml($post->body) !!}
    </article>
@endsection
