@extends('base')

@section('title', 'Home')

@section('page-nav')
    @if (!$posts->onFirstPage())
        <a href="{{ $posts->previousPageUrl() }}">← Previous</a>
    @endif
    @if ($posts->hasMorePages())
        <a href="{{ $posts->nextPageUrl() }}">Next →</a>
    @endif
@endsection

@section('content')
    @if (!$posts->isEmpty())
        @if (!$stickies->isEmpty())
            <section class="stickies">
                <h1>Stickied:</h1>
                @each('post.preview', $stickies, 'post')
            </section>
        @endif
        <section class="latest">
            <h1>Latest:</h1>
            @each('post.preview', $posts, 'post')
        </section>
    @else
        <p>It appears there are no posts to show.</p>
    @endif
@endsection
