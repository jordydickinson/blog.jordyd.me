<?php

namespace App\Policies;

use App\User;
use App\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function view(User $user, Post $post)
    {
        if ($user->id === $post->user_id) {
            return true;
        }

        if ($post->audience === '') {
            return false;
        }

        if ($post->audience === '*') {
            return true;
        }

        if ($post->audience[0] === '^') {
            $user_emails = explode(',', substr($post->audience, 1));
            return !in_array($user->email, $user_emails, true);
        }

        $user_emails = explode(',', $post->audience);
        return in_array($user->email, $user_emails);
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function update(User $user, Post $post)
    {
        return $post->user_id == $user->id;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function delete(User $user, Post $post)
    {
        return $post->user_id == $user->id;
    }
}
