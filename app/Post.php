<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $casts = [
        'user_id' => 'integer'
    ];
    
    protected $fillable = [
        'user_id', 'title', 'preview', 'body', 'sticky', 'audience'
    ];
}
